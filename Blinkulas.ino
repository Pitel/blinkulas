#include <ESP8266WiFi.h>

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  WiFi.softAP("Mikulas", "Blinkulas");
}

void loop() {
  if (WiFi.softAPgetStationNum() > 0) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(1000);
    digitalWrite(LED_BUILTIN, LOW);
    delay(1000);
  } else {
    digitalWrite(LED_BUILTIN, HIGH); // Off
  }
}
