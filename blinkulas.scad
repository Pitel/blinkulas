wall=0.85;

translate([0, -(54 + 2 * wall + 1), 0]) difference() {
    cube([42 + 2 * wall, 54 + 2 * wall, 26 + 1.5 * wall]);
    translate([wall, wall, wall]) cube([42, 54, 26 + wall / 2]);
}

union() {
    cube([42 + 2 * wall, 54 + 2 * wall, wall / 2]);
    translate([wall, wall, 0]) cube([42, 54, wall * 2]);
}